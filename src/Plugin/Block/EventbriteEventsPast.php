<?php

namespace Drupal\eventbrite_events\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eventbrite_events\Eventbrite;

/**
 * Provides a Eventbrite events list Block.
 *
 * @Block(
 *   id = "eventbrite_events_past",
 *   admin_label = @Translation("Eventbrite Past Events block")
 * )
 */
class EventbriteEventsPast extends BlockBase implements BlockPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $events = $this->getEvents();

    if (empty($events)){
      return [
        "#markup" => '<p>' . t('No events to display.') . '</p>',
      ];
    }

    $config = $this->getConfiguration();

    return array(
      '#theme' => [
        'eventbrite_events__'.$config['template_suggestion'],
        'eventbrite_events'
      ],
      '#events' => $events
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['org_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Eventbrite org ID'),
      '#description' => $this->t('Provide a specific Eventbrite org ID, or a token from current node being viewed.'),
      '#required' => TRUE,
      '#default_value' => isset($config['org_id']) ? $config['org_id'] : '',
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['token_tree'] = [
        '#type' => 'container',
        'token_tree_link' => [
          '#theme' => 'token_tree_link',
          '#token_types' => array_keys($this->getTokenData()),
        ],
      ];
    }

    $form['cache_response'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cache JSON response'),
      '#description' => $this->t('If this event is over, there is no need to continue querying against the API.'),
      '#default_value' => !empty($config['cache_response']),
    ];

    $form['cache_length'] = [
      '#type' => 'select',
      '#title' => $this->t('Cache JSON response time length'),
      '#description' => $this->t('If this event is over, there is no need to continue querying against the API.'),
      '#default_value' => !empty($config['cache_length']) ? $config['cache_length']: -1,
      '#options' => [
        -1 => 'Forever',
        1800 => '30 minutes',
        3600 => '1 hour',
        3600 * 24 => '1 day',
      ],
    ];

    $form['template_suggestion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Template suggestion'),
      '#description' => $this->t('Provide a custom suffix for a block template suggestion. Alphanumeric and underscores only.'),
      '#required' => false,
      '#default_value' => isset($config['template_suggestion']) ? $config['template_suggestion'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration['org_id'] = $form_state->getValue('org_id');
    $this->configuration['cache_response'] = $form_state->getValue('cache_response');
    $this->configuration['cache_length'] = $form_state->getValue('cache_length');

    // remove non-alphanumeric and non-underscores
    $template_suggestion = preg_replace('/[^\da-z_]/i', '', $form_state->getValue('template_suggestion'));
    $this->configuration['template_suggestion'] = $template_suggestion;
  }

  /**
   * Get events array
   *
   * @return array
   */
  function getEvents()
  {
    $config = $this->getConfiguration();
    $org_id = $config['org_id'];

    if (\Drupal::moduleHandler()->moduleExists('token')){
      $token = \Drupal::token();
      $org_id = $token->replace($org_id, $this->getTokenData(), [ 'clear' => true ]);
    }

    if (empty($org_id) || !is_numeric($org_id)){
      return [];
    }

    $do_cache = !empty($config['cache_response']);
    $cache_key = "eventbrite_events_block.".md5($org_id.$this->getPluginId());

    if ($do_cache){
      $cache = \Drupal::cache()->get($cache_key);

      if ($cache){
        return $cache->data;
      }
    }

    $events = Eventbrite\Api::getEvents($org_id, 'completed');

    if ($do_cache){
      \Drupal::cache()->set($cache_key, $events, $config['cache_length']);
    }

    return $events;
  }

  /**
   * Returns available context as token data.
   *
   * @return array
   *   An array with token data values keyed by token type.
   */
  protected function getTokenData()
  {
    $data = [];

    $node = \Drupal::routeMatch()->getParameter('node');

    if ($node){
      $data['node'] = $node;
    }

    return $data;
  }
}
