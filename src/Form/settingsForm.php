<?php
namespace Drupal\eventbrite_events\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eventbrite_events\Eventbrite;

class settingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'eventbrite_events_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'eventbrite_events.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $oauth_token = $this->config('eventbrite_events.settings')->get('oauth_token');

    $oauth_token_desc = $this->t('Invalid token');

    if ( $oauth_token ){
      $oauth_token_desc = $this->t('Valid token. This field will not show saved values.');
    }

    $form['eventbrite_events_oauth_token'] = array(
      '#type' => 'password',
      '#title' => $this->t('Personal OAuth Token'),
      '#description' => $oauth_token_desc,
      '#maxlength' => 64,
      '#default_value' => $oauth_token ? $oauth_token : '',
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $valid = Eventbrite\Api::testOauthToken($form_state->getValue('eventbrite_events_oauth_token'));

    if ( !$valid ) {
      $form_state->setErrorByName(
        'eventbrite_events_oauth_token',
        $this->t('Invalid oauth token provided.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('eventbrite_events.settings')
      ->set('oauth_token', $form_state->getValue('eventbrite_events_oauth_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
