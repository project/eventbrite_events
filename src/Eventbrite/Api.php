<?php

namespace Drupal\eventbrite_events\Eventbrite;

use Drupal\Component\Serialization\Json;

class Api {

  /**
   * Build a new array of attendees for an event by querying Eventbrite API
   *
   * @param $event_id
   * @param array $query
   * @return array
   */
  public static function getEvents($org_id, $status = 'live', $query = [ 'page' => 1 ])
  {

    $query['status'] = $status;
    $response = self::query("organizations/{$org_id}/events", $query);

    if (!$response){
      return [];
    }

    $data = Json::decode($response->getBody());

    $events = !empty($data['events']) ? $data['events'] : [];

    // recurse for pagination
    if ($data['pagination']['page_number'] < $data['pagination']['page_count']){
      $events = array_merge($events, self::getEvents($org_id, [
        'page' => $data['pagination']['page_number'] + 1
      ]));
    }

    return $events;
  }

  /**
   * Single request to the Eventbrite API
   *
   * @param $endpoint
   * @param array $query
   * @return mixed
   */
  public static function query($endpoint, $query = [])
  {
    $api_url = "https://www.eventbriteapi.com/v3/{$endpoint}";

    $default_query = [
      'token' => \Drupal::config('eventbrite_events.settings')->get('oauth_token')
    ];

    $response = \Drupal::httpClient()->request('GET', $api_url, [
      'query' => array_replace( $default_query, $query ),
      'http_errors' => FALSE,
    ]);

    if ($response->getStatusCode() == '200'){
      return $response;
    }

    return FALSE;
  }

  /**
   * Determine if the provided oauth token is legitimate
   *
   * @param $token
   * @return bool
   */
  public static function testOauthToken($token)
  {
    $response = self::query('users/me', ['token' => $token ]);

    return $response ? TRUE : FALSE;
  }
}
